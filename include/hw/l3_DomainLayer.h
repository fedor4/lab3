#ifndef LAB3_INCLUDE_HW_L3_DOMAINLAYER_H_
#define LAB3_INCLUDE_HW_L3_DOMAINLAYER_H_
#include "hw/l4_InfrastructureLayer.h"

const int MAX_NAME = 70;
const int MAX_MANUFACTUR = 70;
const int MAX_MODEL = 70;
const int MIN_PRICE = 0;
const int MAX_PRICE = 1000000000;

class MusicalInstrumentStore : public ICollectable {
 protected:
  bool invariant() const;
 public:
  MusicalInstrumentStore() = delete;
  MusicalInstrumentStore(const MusicalInstrumentStore& p) = delete;
  MusicalInstrumentStore& operator=(const MusicalInstrumentStore& p) = delete;
  MusicalInstrumentStore(std::string_view name, std::string_view manufacturer, std::string_view model, int price, bool exist);
  const std::string& GetName() const;
  const std::string& GetManufacturer() const;
  const std::string& GetModel() const;
  std::string GetPrice() const;
  std::string GetExist() const;
  bool write(std::ostream& os) override;
 private:
  std::string _name;
  std::string _manufacturer;
  std::string _model;
  int _price;
  bool _exist;
};

class ItemCollector: public ACollector {
 public:
  virtual std::shared_ptr<ICollectable> read(std::istream& is) override;
};

#endif //LAB3_INCLUDE_HW_L3_DOMAINLAYER_H_
