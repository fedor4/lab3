#include "hw/l3_DomainLayer.h"

bool MusicalInstrumentStore::invariant() const {
  return _name.size() <= MAX_NAME &&
	  _manufacturer.size() <= MAX_MANUFACTUR &&
	  _model.size() <= MAX_MODEL &&
	  _price >= MIN_PRICE && _price <= MAX_PRICE;
}

MusicalInstrumentStore::MusicalInstrumentStore(std::string_view name,
std::string_view manufacturer,
	std::string_view model,
int price,
bool exist):
_name(name),
_manufacturer(manufacturer),
_model(model),
_price(price),
_exist(exist) {
  assert(invariant());
}

const std::string& MusicalInstrumentStore::GetName() const{
  return _name;
}

const std::string& MusicalInstrumentStore::GetManufacturer() const{
  return _manufacturer;
}

const std::string& MusicalInstrumentStore::GetModel() const{
  return _model;
}

std::string MusicalInstrumentStore::GetPrice() const{
  return std::to_string(_price);
}

std::string MusicalInstrumentStore::GetExist() const{
  return _exist ?  "В наличие" : "Нет в продаже";
}

bool MusicalInstrumentStore::write(std::ostream& os) {
  writeString(os, _name);
  writeString(os, _manufacturer);
  writeString(os, _model);
  writeNumber(os, _price);
  writeNumber(os, _exist);
  return os.good();
}


std::shared_ptr<ICollectable> ItemCollector::read(std::istream &is) {
  std::string name = readString(is, MAX_NAME);
  std::string manufacturer = readString(is, MAX_MANUFACTUR);
  std::string model = readString(is, MAX_MODEL);
  int price = readNumber<int>(is);
  bool exist = readNumber<bool>(is);
  return std::make_shared<MusicalInstrumentStore>(name, manufacturer, model, price, exist);
}


